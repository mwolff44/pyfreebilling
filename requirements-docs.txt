mkdocs==1.5.3
mkdocs-material==9.5.3
mkdocs-static-i18n==0.56
mkdocs-render-swagger-plugin==0.1.1
mkdocs-git-revision-date-localized-plugin==1.2.2
mkdocs-static-i18n[material]
mkdocs-rss-plugin==1.12.0